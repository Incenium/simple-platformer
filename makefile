CFLAGS = -std=c99 -Wall -g -o game -lSDL -lSDL_image

all:
	cd src; clang -c *.c -g; mv *.o ../obj;
	cd obj; clang *.o $(CFLAGS); mv game ..;

clean:
	rm -f a.out
