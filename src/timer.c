#include "include/timer.h"

void timer_init(Timer *timer)
{
	timer->startTicks = 0;
	timer->pausedTicks = 0;
	timer->paused = false;
	timer->started = false;
}

void timer_start(Timer *timer)
{
	timer->started = true;
	timer->paused = false;
	timer->startTicks = SDL_GetTicks();
}

void timer_stop(Timer *timer)
{
	timer->started = false;
	timer->paused = false;
}

void timer_pause(Timer *timer)
{
	if ((timer->started == true) && (timer->paused == false)){
		timer->started = true;

		timer->pausedTicks = SDL_GetTicks() - timer->startTicks;
	}
}

void timer_unpause(Timer *timer)
{
	if (timer->paused == true){
		timer->paused = false;

		timer->startTicks = SDL_GetTicks() - timer->pausedTicks;

		timer->pausedTicks= 0;
	}
}

int timer_get_ticks(Timer *timer)
{
	if (timer->started == true){
		if (timer->paused == true){
			return timer->pausedTicks;
		}
		
		else {
			return SDL_GetTicks() - timer->startTicks;
		}
	}

	return 0;
}

bool timer_is_started(Timer *timer)
{
	return timer->started;
}

bool timer_is_paused(Timer *timer)
{
	return (*timer).paused;
}
