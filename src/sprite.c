#include "include/sprite.h"

int up = 0;
int left = 1;
int right = 2;
int down = 3;

/* Add int width, int height to init to set collision box */
void sprite_init(Sprite *sprite, int x, int y, SDL_Surface *image)
{
    sprite->box.x = x;
    sprite->box.y = y;
    sprite->box.w = image->w;
    sprite->box.h = image->h;
    sprite->image = image;
}

void sprite_handleInput(Sprite *sprite, SDL_Event event)
{
	if (event.type == SDL_KEYDOWN){
		switch (event.key.keysym.sym){
			case SDLK_UP: sprite->yVel -= 4 / 2; break;
			case SDLK_DOWN: sprite->yVel += 4 / 2; break;
			case SDLK_LEFT: sprite->xVel -= 4 / 2; break;
			case SDLK_RIGHT: sprite->xVel += 4 / 2; break;
			default : ;
		}
	}

	else if (event.type == SDL_KEYUP){
		switch (event.key.keysym.sym){
			case SDLK_UP: sprite->yVel += 4 / 2; break;
			case SDLK_DOWN: sprite->yVel -= 4 / 2; break;
			case SDLK_LEFT: sprite->xVel += 4 / 2; break;
			case SDLK_RIGHT: sprite->xVel -= 4 / 2; break;
			default : ;
		}
	}
}

void sprite_move(Sprite *sprite, Level *level)
{
    sprite->box.x += sprite->xVel;

    if ((sprite->box.x < 0) || (sprite->box.x + 4 / 2 > level->levelWidth) || (touches_wall(sprite->box, level->map, level->numTiles))){
        
        sprite->box.x -= sprite->xVel;
    }

    sprite->box.y += sprite->yVel;

    if ((sprite->box.y < 0) || (sprite->box.y + 4 / 2 > level->levelWidth) || (touches_wall(sprite->box, level->map, level->numTiles))){
        
        sprite->box.y -= sprite->yVel;
    }
}

void sprite_show(Sprite *sprite, SDL_Surface *screen)
{
    /* The NULL there will go when we get spritesheets going */
    apply_surface(sprite->box.x, sprite->box.y, sprite->image, screen, NULL);
}
/*
void sprite_setCamera(Sprite *sprite, SDL_Rect camera)
{
	extern int SCREEN_WIDTH;
	extern int SCREEN_HEIGHT;
	
	camera.x = (sprite->box.x + 4 / 2) - SCREEN_WIDTH / 2;
	camera.y = (sprite->box.y + 4 / 2) - SCREEN_HEIGHT / 2;
	
	if (camera.x < 0){
		camera.x = 0;
	}
	
	if (camera.y < 0){
		camera.y = 0;
	}
	
	if (camera.x > LEVEL_WIDTH - camera.w){
		camera.x = LEVEL_WIDTH - camera.w;
	}
	
	if (camera.y > LEVEL_HEIGHT - camera.h){
		camera.y = LEVEL_WIDTH - camera.h;
	}
}
*/
void sprite_destroy(Sprite *sprite)
{
	SDL_FreeSurface(sprite->image);
	free(sprite);
	sprite = NULL;
}
