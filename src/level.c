#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "SDL/SDL.h"
#include "include/load_util.h"
#include "include/level.h"
#include "include/tile.h"

void level_init(Level *level, int numTiles, int levelWidth, int levelHeight, char *filename)
{
    /* Level width/height is given in tiles which is then
       calculated into pixel width/height for collision
       detection */

    level->numTiles = numTiles;

    level->map = NULL;
    level->map = (Tile *) malloc(numTiles * sizeof(Tile));

    level->levelWidth = levelWidth * TILE_WIDTH;
    level->levelHeight = levelHeight * TILE_HEIGHT;

    set_tiles(filename, levelWidth, level->map);
}

void level_show(Level *level, SDL_Surface *screen, SDL_Surface *tileSheet, SDL_Rect clips[])
{
    assert(level != NULL && screen != NULL && tileSheet != NULL);
    
    for (int i = 0; i < level->numTiles; i++){
        tile_show(&level->map[i], screen, tileSheet, &clips[tile_getType(&level->map[i])]);
    }
}

void level_destroy(Level *level)
{
    assert(level != NULL);

    int i = 0;
    for (i = 0; i < level->numTiles; i++){
        tile_destroy(&level->map[i]);
    }
    free(level->map);
    level->map = NULL;
}
