#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "include/draw_util.h"
#include "include/timer.h"
#include "include/sprite.h"
#include "include/level.h"
#include "include/load_util.h"
#include <stdio.h>
#include <stdbool.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;

const int FPS = 60;

SDL_Surface *screen = NULL;
SDL_Surface *spriteImage = NULL;
SDL_Surface *tileSheet = NULL;
//SDL_Surface *tileBrick = NULL;
//SDL_Surface *tileBlock = NULL;

SDL_Event event;

Timer timer;

Sprite sprite;

Level level;

SDL_Rect clips[2];

SDL_Rect camera;

/*SDL_Surface* load_image(char filename[])
{
    SDL_Surface *loadedImage = NULL;
    SDL_Surface *optimizedImage = NULL;

    loadedImage = IMG_Load(filename);

    if (loadedImage != NULL){
        optimizedImage = SDL_DisplayFormatAlpha(loadedImage);
        SDL_FreeSurface(loadedImage);
    }

    return optimizedImage;
}*/

bool load_files()
{
    spriteImage = load_image("assets/misc/dot.png");
    tileSheet = load_image("assets/misc/tiles.png");
    //tileBrick = load_image("assets/misc/brick.gif");
    //tileBlock = load_image("assets/misc/block.gif");

    if (spriteImage == NULL){
        return false;
    }

    if (tileSheet == NULL){
        return false;
    }
    
    return true;
}

bool init()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1){
        printf("Could not initialize everything");
        return false;
    }

    /*if (TTF_Init() == -1){
        printf("Could not initialize TTF");
        return false;
    }*/

    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_HWSURFACE|SDL_DOUBLEBUF);

    if (screen == NULL){
        printf("screen is null");
        return false;
    }

    SDL_WM_SetCaption("Simple platformer", NULL);

    return true;
}

/* all game object clean_up methods are called here */
void clean_up()
{
    //level_destroy(&level);

    SDL_Quit();
}

void update()
{

}

void draw()
{

}

void clip_tiles()
{
    //Clip the sprite sheet
    printf("%d, %d\n", TILE_BUSH_0, TILE_BUSH_1);
    clips[ TILE_BUSH_0 ].x = 0;
    clips[ TILE_BUSH_0 ].y = 0;
    clips[ TILE_BUSH_0 ].w = TILE_WIDTH;
    clips[ TILE_BUSH_0 ].h = TILE_HEIGHT;

    clips[ TILE_BUSH_1 ].x = 32;
    clips[ TILE_BUSH_1 ].y = 0;
    clips[ TILE_BUSH_1 ].w = TILE_WIDTH;
    clips[ TILE_BUSH_1 ].h = TILE_HEIGHT;
}

int main(int argc, char *args[])
{
    camera.x = 0;
    camera.y = 0;
    camera.w = SCREEN_WIDTH;
    camera.h = SCREEN_HEIGHT;

    int frame = 0;
    bool quit = false;
    
    if (init() == false){
        return 1;
    }

    if (load_files() == false){
        return 1;
    }

    sprite_init(&sprite, 66, 32, spriteImage);

    clip_tiles();

    level_init(&level, 196, 14, 14, "assets/misc/testmap.tmx.txt");

    while (quit == false){
        timer_start(&timer);
        
        while (SDL_PollEvent(&event)){
            if (event.type == SDL_QUIT){
                quit = true;
            }

            sprite_handleInput(&sprite, event);
        }

        /* Sprite doesn't move because he is outside
           of the level width and height */
        sprite_move(&sprite, &level);
        //sprite_setCamera(&sprite, camera);

        level_show(&level, screen, tileSheet, clips);

        sprite_show(&sprite, screen);

        if (SDL_Flip(screen) == -1){
            printf("Could not flip screen\n");
            return 1;
        }

        if (timer_get_ticks(&timer) < 1000 / FPS){
            SDL_Delay((1000 / FPS) - timer_get_ticks(&timer));
        }
        /* TEST CODE, REMOVE AFTER */
        frame++;
        if (frame == FPS){
            printf("%d\n", frame);
            frame = 0;
        }
    }

    clean_up();

    return 0;
}
