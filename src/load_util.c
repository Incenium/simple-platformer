#include <stdlib.h>
#include <stdio.h>
#include "include/load_util.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

SDL_Surface* load_image(char filename[])
{
    SDL_Surface *loadedImage = NULL;
    SDL_Surface *optimizedImage = NULL;

    loadedImage = IMG_Load(filename);

    if (loadedImage != NULL){
        optimizedImage = SDL_DisplayFormatAlpha(loadedImage);
        SDL_FreeSurface(loadedImage);
    }
    
    else {
    	printf("could not load the image at %s", filename);
    	exit(1);
    }

    return optimizedImage;
}
