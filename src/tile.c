#include <stdio.h>
#include "SDL/SDL.h"
#include "include/tile.h"
#include "include/collision.h"
#include "include/draw_util.h"

/* There can be problems with maps
   being bigger than the array which
   results in a segmentation fault */

int TILE_WIDTH = 32;
int TILE_HEIGHT = 32;

int TILE_BUSH_0 = 0;
int TILE_BUSH_1 = 1;

void tile_init(Tile *tile, int x, int y, int tileType)
{
    tile->box.x = x;
    tile->box.y = y;

    tile->box.w = TILE_WIDTH;
    tile->box.h = TILE_HEIGHT;

    tile->type = tileType;
}

void tile_show(Tile *tile, SDL_Surface *screen, SDL_Surface *tileSheet, SDL_Rect *clip)
{
	apply_surface(tile->box.x, tile->box.y, tileSheet, screen, clip);
}

/* Width in tiles */
bool set_tiles(char *filename, int width, Tile *tiles)
{
    int x = 0;
    int y = 0;

    FILE *file = NULL;

    file = fopen(filename, "r");

    if (!file){
        printf("Could not load tile map");
        return false;
    }

    int i = 0;
    /* TEST CODE, REMOVE AFTERWARDS ANDREW THE FORGETFULL */
    char buf[5000];

    while (fgets(buf, 5000, file) != NULL){
        char *token = strtok(buf, " ");
        while (token){
            int tileType = atoi(token);

            if ((tileType >= 0) && (tileType < 2)){
                tile_init(&tiles[i], x, y, tileType);
            }

            else {
                fclose(file);
                return false;
            }

            x += TILE_WIDTH;

            if (x >= TILE_WIDTH * width){
                x = 0;
                y += TILE_HEIGHT;
            }

            token = strtok(NULL, " ");
            i++;
        }
        
    }

    fclose(file);
    
    return true;
}

int tile_getType(Tile *tile)
{
    return tile->type;
}

SDL_Rect tile_getBox(Tile *tile)
{
    return tile->box;
}

void tile_destroy(Tile *tile)
{
    free(tile);
    tile = NULL;
}
