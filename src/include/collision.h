#include "SDL/SDL.h"
#include "tile.h"
#include <stdbool.h>

#ifndef COLLISION_H_INCLUDED
#define COLLISION_H_INCLUDED

bool check_collision(SDL_Rect A, SDL_Rect B);
bool touches_wall(SDL_Rect box, Tile *tiles, int totalTiles);

#endif
