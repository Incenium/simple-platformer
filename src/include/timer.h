#include "SDL/SDL.h"
#include <stdbool.h>
#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED
typedef struct Timer{
	int startTicks;
	int pausedTicks;

	bool paused;
	bool started;
} Timer;

void timer_init(Timer *timer);
void timer_start(Timer *timer);
void timer_stop(Timer *timer);
void timer_pause(Timer *timer);
void timer_unpause(Timer *timer);

int timer_get_ticks(Timer *timer);

bool timer_is_started(Timer *timer);
bool timer_is_paused(Timer *timer);
#endif