#include "SDL/SDL.h"
#include <stdbool.h>

#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED
extern int TILE_WIDTH;
extern int TILE_HEIGHT;

extern int TILE_BUSH_0;
extern int TILE_BUSH_1;

typedef struct Tile
{
	SDL_Rect box;
	
	int type;
} Tile;

void tile_init(Tile *tile, int x, int y, int tileType);
void tile_show(Tile *tile, SDL_Surface *screen, SDL_Surface *tileSheet, SDL_Rect clips[]);
bool set_tiles(char *filename, int width, Tile *tiles);
int tile_getType(Tile *tile);
SDL_Rect tile_getBox(Tile *tile);
void tile_destroy(Tile *tile);
#endif
