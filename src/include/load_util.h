#include "SDL/SDL.h"

#ifndef LOAD_UTIL_INCLUDED
#define LOAD_UTIL_INCLUDED
SDL_Surface* load_image(char filename[]);
#endif
