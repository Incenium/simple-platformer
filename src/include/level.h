#include <stdio.h>
#include "tile.h"

#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED
typedef struct Level
{
	int numTiles;
	Tile *map;
	
	int levelWidth;
	int levelHeight;
} Level;

void level_init(Level *level, int numTiles, int levelWidth, int levelHeight, char *levelname);
void level_show(Level *level, SDL_Surface *screen, SDL_Surface *tileSheet, SDL_Rect clips[]);
void level_destroy(Level *level);
#endif
