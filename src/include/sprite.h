#include "SDL/SDL.h"
#include "draw_util.h"
#include "collision.h"
#include "level.h"

#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

int up;
int left;
int right;
int down;

typedef struct Sprite
{
	SDL_Rect box;

	int velocity;
	int status;
	int xVel, yVel;

	SDL_Surface *image;
} Sprite;

void sprite_init(Sprite *sprite, int x, int y, SDL_Surface *image);
void sprite_handleInput(Sprite *sprite, SDL_Event event);
void sprite_move(Sprite *sprite, Level *level);
void sprite_show(Sprite *sprite, SDL_Surface *screen);
//void sprite_setCamera(Sprite *sprite,SDL_Rect camera);
void sprite_destroy(Sprite *sprite);
#endif
