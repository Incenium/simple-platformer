#include "SDL/SDL.h"

#ifndef DRAWUTIL_H_INCLUDED
#define DRAWUTIL_H_INCLUDED

void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);

#endif
