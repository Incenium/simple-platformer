#include <stdio.h>
#include "include/collision.h"
#include "include/tile.h"

bool check_collision(SDL_Rect A, SDL_Rect B)
{
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	leftA = A.x;
	rightA = A.x + A.w;
	topA = A.y;
	bottomA = A.y + A.h;

	leftB = B.x;
	rightB = B.x + B.w;
	topB = B.y;
	bottomB = B.y + B.h;

	if (bottomA <= topB){
		return false;
	}

	if (topA >= bottomB){
		return false;
	}

	if (rightA <= leftB){
		return false;
	}

	if (leftA >= rightB){
		return false;
	}

	return true;
}

/* Tile specifique, don't know if touches wall should take a Level struct as param */
bool touches_wall(SDL_Rect box, Tile *tiles, int totalTiles)
{
	int t = 0;
		
	for (t = 0; t < totalTiles; t++){
		if (tile_getType(&tiles[t]) == 1){ /* TEST CODE HERE, CHANGE LATER */
			if (check_collision(box, tile_getBox(&tiles[t])) == true){
				return true;
			}
		}
	}
	
	return false;
}
